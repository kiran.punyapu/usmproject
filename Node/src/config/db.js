const {Sequelize} = require('sequelize');

const sequelize = new Sequelize({
    database:'usmtest',
    username:'root',
    password:'root',
    port:'3306',
    dialect:'mysql'
});

dbConnection = async ()=>{
    try{
        await sequelize.authenticate();
        
        console.log('database connected successfully')
      }catch(err){
          console.log('error occured while connecting to database',err);
      }
}

 module.exports = {sequelize,dbConnection}; // For Mysql Connection
