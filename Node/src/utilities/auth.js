const {verify,sign} = require('jsonwebtoken');
const JWT_KEY="task$123"

module.exports.jwtSign=(userObj)=>{
return sign(userObj,JWT_KEY);
}

module.exports.verifyToken=(token)=>{
    return verify(token,JWT_KEY);
}