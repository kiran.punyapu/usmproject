const {randomBytes,scrypt} = require('crypto');
const { promisify } = require('util');
const scryptAsync = promisify(scrypt);


module.exports.toHash=async (password)=>{
   const salt =  randomBytes(8).toString('hex');
   const buffer = await scryptAsync(password,salt,64);
   return `${buffer.toString('hex')}.${salt}`;
}

module.exports.comparePassword=async(storedPassword,password)=>{
    const [hashedPassword,salt]=storedPassword.split('.');
    const buffer = await scryptAsync(password,salt,64);
    return hashedPassword==buffer.toString('hex'); 
}

