const express = require('express');
const app = express();
const cors = require('cors');
const port = process.env.PORT || 8000;
const router = require('./src/routes/User');
const cookieParser = require('cookie-parser');
 const db = require('./src/config/db').dbConnection(); // mysql connection
 const cookiesession = require('cookie-session')

app.use(cookiesession({signed:false,secure:false}))
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cookieParser());

app.use('/api/User',router);
app.listen(port,()=>{
  console.log(`server listening at ${port}`)
});