import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { FormGroup ,FormControl} from '@angular/forms';


@Injectable({
  providedIn: 'root'
})
export class TaskserviceService {

  constructor(private http: HttpClient) { }

  login(loginData): Observable<any> {
    return this.http.post(
      "http://localhost:8000/api/User/signin",
      loginData,{responseType: 'text'}
    );
  }

  
  userregister(registerData): Observable<any> {
    return this.http.post(
      "http://localhost:8000/api/User/signup",
      registerData,{responseType: 'text'}
    );
  }

  userdata(emaildata):Observable<any>{
    return this.http.post(
      "http://localhost:8000/api/User/getbyemail",emaildata,{responseType: 'text'}
    );
  }

  alluserdata():Observable<any>{
    return this.http.get(
      "http://localhost:8000/api/User/getall"
    );
  }

  deleteuserid(deldata): Observable<any> {
    return this.http.post(
      "http://localhost:8000/api/User/deleteuser",
      deldata,{responseType: 'text'}
    );
  }



  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

}
